package dev.agno.reservations.integration;

import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.repositories.DatastoreRepository;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

import static dev.agno.reservations.Constants.ONE_DAY;
import static dev.agno.reservations.Constants.THREE_DAYS;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

public class TestDatastoreRepository {
    private ArrayList<String> addedKeys = new ArrayList<>();

    private static long getNextMidnightTime() {
        return getMidnightStartTimeAfter(Instant.now().getEpochSecond() + ONE_DAY);
    }

    private static long getMidnightStartTimeAfter(long initialStartTime) {
        return Instant.ofEpochSecond(initialStartTime + ONE_DAY).truncatedTo(ChronoUnit.DAYS).getEpochSecond();
    }

    @BeforeClass
    public static void ensureDatastoreAvailable() {
        // You should run the local emulated datastore:
        // /usr/lib/google-cloud-sdk/platform/cloud-datastore-emulator/cloud_datastore_emulator start --host=localhost --port=8081 --store_on_disk=False /opt/.config/gcloud/emulators/datastore
        org.junit.Assume.assumeTrue(isDatastoreAvailable());
    }

    public static boolean isDatastoreAvailable() {
        try {
            String dontcare = "DONTCARE";
            DatastoreRepository repo = new DatastoreRepository(true);
            repo.deleteReservation(dontcare);
        } catch(Exception e) {
            return false;
        }
        return true;
    }

    @After
    public void clearKeys() {
        DatastoreRepository repo = new DatastoreRepository(true);
        for(var s : this.addedKeys) {
            repo.deleteReservation(s);
        }
    }

    @Test
    public void addReservation() {
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(getNextMidnightTime());
        r.setExtent(ONE_DAY);
        String k = repo.addReservation(r, r.getStartTime(), r.getExtent());
        assertThat(k, is(not(nullValue())));
        addedKeys.add(k);
    }

    @Test
    public void addDuplicateReservation_fails() {
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(getNextMidnightTime());
        r.setExtent(ONE_DAY);
        String key = repo.addReservation(r, r.getStartTime(), r.getExtent());
        assertThat(key, is(not(nullValue())));
        addedKeys.add(key);
        key = repo.addReservation(r, r.getStartTime(), r.getExtent());
        assertThat(key, is(nullValue()));
    }

    @Test
    public void TestGetAllReservations() {
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(getNextMidnightTime());
        r.setExtent(ONE_DAY);
        String key = repo.addReservation(r, r.getStartTime(), r.getStartTime() + r.getExtent());
        assertThat(key, is(not(nullValue())));
        addedKeys.add(key);
        var reservations = repo.getReservations(r.getStartTime(), r.getExtent());
        assertThat(reservations, hasSize(1));
        assertThat(reservations, hasItems(r));
    }

    @Test
    public void TestGetOneReservation() {
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(getNextMidnightTime());
        r.setExtent(ONE_DAY);
        String key = repo.addReservation(r, r.getStartTime(), r.getStartTime() + r.getExtent());
        assertThat(key, is(not(nullValue())));
        addedKeys.add(key);
        var reservation = repo.getReservation(key);
        assertThat(reservation.getStartTime(), is(r.getStartTime()));
    }

    @Test
    public void TestGetMultipleReservations() {
        final long earliestStartTime = getNextMidnightTime();
        final long latestSearchTime = earliestStartTime + 2 * ONE_DAY;
        final long firstIntervalExtent = ONE_DAY;
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String key = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(key, is(not(nullValue())));
        addedKeys.add(key);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent);
        key = repo.addReservation(r, earliestStartTime, latestSearchTime);
        addedKeys.add(key);
        var reservations = repo.getReservations(earliestStartTime, latestSearchTime);
        assertThat(reservations, hasSize(2));
    }

    @Test
    public void TestDeleteNonExistentReservation() {
        final String nonExistentKey = "hello";
        DatastoreRepository repo = new DatastoreRepository(true);
        assertThat(repo.deleteReservation(nonExistentKey), is(false));
    }

    @Test
    public void TestUpdateReservationNonExistentKey() {
        final String nonExistentKey = "hello";
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setExtent(getNextMidnightTime());
        r.setStartTime(ONE_DAY);
        assertThat(repo.updateReservation(nonExistentKey, r, THREE_DAYS), is(false));
    }

    @Test
    public void TestUpdateReservationTooManyOverlaps() {
        final long earliestStartTime = getNextMidnightTime();
        final long firstIntervalExtent = 2 * ONE_DAY;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long thirdIntervalStartTime = earliestStartTime + 2 * firstIntervalExtent;
        final long updateStartTime = earliestStartTime + ONE_DAY;
        final long updateExtent = 3 * ONE_DAY;
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent); // 0-2
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(firstKey, is(notNullValue()));
        addedKeys.add(firstKey);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent); // 2-4
        String secondKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(secondKey, is(notNullValue()));
        addedKeys.add(secondKey);
        r.setStartTime(thirdIntervalStartTime); // 4-6
        String thirdKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(thirdKey, is(notNullValue()));
        addedKeys.add(thirdKey);
        var reservations = repo.getReservations(earliestStartTime, latestSearchTime);
        assertThat(reservations, hasSize(3));
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(thirdKey, r, THREE_DAYS), is(false));
    }

    @Test
    public void TestUpdateReservationSingleOverlapUpdate() {
        final long earliestStartTime = getNextMidnightTime();
        final long firstIntervalExtent = ONE_DAY;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long updateStartTime = 3 * ONE_DAY;
        final long updateExtent = firstIntervalExtent;
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(firstKey, is(notNullValue()));
        addedKeys.add(firstKey);
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(firstKey, r, THREE_DAYS), is(true));
    }

    @Test
    public void TestUpdateReservationNoOverlap() {
        final long earliestStartTime = getNextMidnightTime();
        final long firstIntervalExtent = ONE_DAY;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long updateStartTime = earliestStartTime + ONE_DAY;
        final long updateExtent = firstIntervalExtent;
        DatastoreRepository repo = new DatastoreRepository(true);
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(firstKey, is(notNullValue()));
        addedKeys.add(firstKey);
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(firstKey, r, THREE_DAYS), is(true));
    }
}
