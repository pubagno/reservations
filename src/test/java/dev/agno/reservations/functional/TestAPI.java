package dev.agno.reservations.functional;

import dev.agno.reservations.ReservationRouter;
import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.repositories.InMemoryReservationRepository;
import dev.agno.reservations.repositories.ReservationRepository;
import dev.agno.reservations.services.DefaultReservationService;
import dev.agno.reservations.services.ReservationService;
import io.javalin.Javalin;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import kong.unirest.ObjectMapper;
import kong.unirest.Unirest;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.concurrent.NotThreadSafe;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;

import static dev.agno.reservations.Constants.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TestAPI {
    Javalin app;
    ReservationRepository repository;
    ReservationService service;
    ReservationRouter router;

    @Before
    public void setup() {
        app = Javalin.create();
        repository = new InMemoryReservationRepository();
        service = new DefaultReservationService(repository);
        router = new ReservationRouter(app, service);
        router.bindRoutes();
    }

    private static String makeResourceURLNoKey(int port) {
        return makeResourceURL(port, null);
    }

    private static String makeResourceURL(int port, String key) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("http://localhost:");
        urlBuilder.append(port);
        urlBuilder.append("/");
        urlBuilder.append(RESERVATIONS_PREFIX);
        if (key != null) {
            urlBuilder.append("/");
            urlBuilder.append(key);
        }
        return urlBuilder.toString();
    }

    private static long getNextMidnightTime() {
        return getMidnightStartTimeAfter(Instant.now().getEpochSecond() + ONE_DAY);
    }

    private static long getMidnightStartTimeAfter(long initialStartTime) {
        return Instant.ofEpochSecond(initialStartTime + ONE_DAY).truncatedTo(ChronoUnit.DAYS).getEpochSecond();
    }

    @Test
    public void POST_reservation_returns_key() {
        app.start(0);
        String url = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(url)
                .queryString(PARAM_STARTTIME, getNextMidnightTime())
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        app.stop();
    }

    @Test
    public void POST_reservation_early_startTime_fails() {
        app.start(0);
        String url = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(url)
                .queryString(PARAM_STARTTIME, getMidnightStartTimeAfter(Instant.now().getEpochSecond() - ONE_DAY))
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.BAD_REQUEST_400));
        app.stop();
    }

    @Test
    public void POST_reservation_oneMonth_startTime_fails() {
        app.start(0);
        String url = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(url)
                .queryString(PARAM_STARTTIME, getMidnightStartTimeAfter(Instant.now().getEpochSecond() + 60 * ONE_DAY))
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.BAD_REQUEST_400));
        app.stop();
    }

    @Test
    public void POST_reservation_nonMidnight_startTime_fails() {
        app.start(0);
        String url = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(url)
                .queryString(PARAM_STARTTIME, getNextMidnightTime() + 1)
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.BAD_REQUEST_400));
        app.stop();
    }

    @Test
    public void POST_reservation_nonDay_extent_fails() {
        app.start(0);
        String url = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(url)
                .queryString(PARAM_STARTTIME, getNextMidnightTime())
                .queryString(PARAM_EXTENT, ONE_DAY + 1)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.BAD_REQUEST_400));
        app.stop();
    }

    @Test
    public void POST_reservation_greater3Day_extent_fails() {
        app.start(0);
        String url = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(url)
                .queryString(PARAM_STARTTIME, getNextMidnightTime())
                .queryString(PARAM_EXTENT, THREE_DAYS + ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.BAD_REQUEST_400));
        app.stop();
    }

    @Test
    public void GET_reservation_noSuchReservation() {
        final String nonExistentKey = "no-such-key";
        app.start(0);
        String url = makeResourceURL(app.port(), nonExistentKey);
        HttpResponse<String> response = Unirest.get(url).asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.NOT_FOUND_404));
        app.stop();
    }

    @Test
    public void GET_reservation_succeeds() {
        app.start(0);
        long startTime = getNextMidnightTime();
        String postUrl = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime)
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String key = response.getBody();
        String url = makeResourceURL(app.port(), key.replaceAll("^\"|\"$", ""));
        HttpResponse<JsonNode> getResponse = Unirest.get(url).asJson();
        assertThat(getResponse.getBody().toString(), getResponse.getStatus(), is(HttpStatus.OK_200));
        JSONObject responseObject = getResponse.getBody().getObject();
        assertThat(responseObject.get(PARAM_STARTTIME), equalTo((int)startTime));
        assertThat(responseObject.get(PARAM_EXTENT), equalTo((int)ONE_DAY));
        app.stop();
    }

    @Test
    public void GET_reservations_succeeds() {
        app.start(0);
        long startTime = getNextMidnightTime();
        String postUrl = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime)
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String firstKey = response.getBody();

        response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime + ONE_DAY)
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String secondKey = response.getBody();
        assertThat(firstKey, is(not(secondKey)));

        String url = makeResourceURLNoKey(app.port());
        HttpResponse<JsonNode> getResponse = Unirest.get(url).asJson();
        assertThat(getResponse.getBody().toString(), getResponse.getStatus(), is(HttpStatus.OK_200));
        assertThat(getResponse.getBody().isArray(), is(true));
        JSONArray responseArray = getResponse.getBody().getArray();
        assertThat(responseArray.length(), is(2));
        JSONObject response1 = responseArray.getJSONObject(0);
        JSONObject response2 = responseArray.getJSONObject(1);
        assertThat(response1.getInt(PARAM_STARTTIME), is(oneOf((int)(startTime + ONE_DAY), (int)startTime)));
        assertThat(response2.getInt(PARAM_STARTTIME), is(oneOf((int)(startTime + ONE_DAY), (int)startTime)));
        assertThat(response1.getInt(PARAM_STARTTIME), is(not(response2.getInt(PARAM_STARTTIME))));
        app.stop();
    }

    @Test
    public void PATCH_reservation_succeeds() {
        app.start(0);
        long startTime = getNextMidnightTime();
        long initialExtent = 2 * ONE_DAY;
        String postUrl = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String key = response.getBody().replaceAll("^\"|\"$", "");

        String patchUrl = makeResourceURL(app.port(), key);
        long updatedStart = startTime + ONE_DAY;
        response = Unirest.patch(patchUrl)
                .queryString(PARAM_STARTTIME, updatedStart)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.NO_CONTENT_204));

        HttpResponse<JsonNode> getResponse = Unirest.get(patchUrl).asJson();
        assertThat(getResponse.getBody().toString(), getResponse.getStatus(), is(HttpStatus.OK_200));
        JSONObject responseObject = getResponse.getBody().getObject();
        assertThat(responseObject.get(PARAM_STARTTIME), equalTo((int)updatedStart));
        assertThat(responseObject.get(PARAM_EXTENT), equalTo((int)initialExtent));
        app.stop();
    }

    @Test
    public void PATCH_reservation_conflicts() {
        app.start(0);
        long startTime = getNextMidnightTime();
        long initialExtent = 2 * ONE_DAY;
        long updatedStart = startTime + ONE_DAY;
        String postUrl = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String firstKey = response.getBody().replaceAll("^\"|\"$", "");

        response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime + initialExtent)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));

        String patchUrl = makeResourceURL(app.port(), firstKey);
        response = Unirest.patch(patchUrl)
                .queryString(PARAM_STARTTIME, updatedStart)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CONFLICT_409));

        HttpResponse<JsonNode> getResponse = Unirest.get(patchUrl).asJson();
        assertThat(getResponse.getBody().toString(), getResponse.getStatus(), is(HttpStatus.OK_200));
        JSONObject responseObject = getResponse.getBody().getObject();
        assertThat(responseObject.get(PARAM_STARTTIME), equalTo((int)startTime));
        assertThat(responseObject.get(PARAM_EXTENT), equalTo((int)initialExtent));
        app.stop();
    }

    @Test
    public void PATCH_reservation_notfound() {
        app.start(0);
        long startTime = getNextMidnightTime();
        long initialExtent = 2 * ONE_DAY;
        long updatedStart = startTime + ONE_DAY;
        String postUrl = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String firstKey = response.getBody().replaceAll("^\"|\"$", "");
        String mangledKey = firstKey + "abc";

        String patchUrl = makeResourceURL(app.port(), mangledKey);
        response = Unirest.patch(patchUrl)
                .queryString(PARAM_STARTTIME, updatedStart)
                .queryString(PARAM_EXTENT, initialExtent)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.NOT_FOUND_404));
        app.stop();
    }

    @Test
    public void DELETE_reservation_fails() {
        final String nonExistentKey = "no-such-key";
        app.start(0);
        String url = makeResourceURL(app.port(), nonExistentKey);
        HttpResponse<String> response = Unirest.delete(url).asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.NOT_FOUND_404));
        app.stop();
    }

    @Test
    public void DELETE_reservation_succeeds() {
        app.start(0);
        long startTime = getNextMidnightTime();
        String postUrl = makeResourceURLNoKey(app.port());
        HttpResponse<String> response = Unirest.post(postUrl)
                .queryString(PARAM_STARTTIME, startTime)
                .queryString(PARAM_EXTENT, ONE_DAY)
                .asString();
        assertThat(response.getBody(), response.getStatus(), is(HttpStatus.CREATED_201));
        String key = response.getBody();
        String url = makeResourceURL(app.port(), key.replaceAll("^\"|\"$", ""));
        response = Unirest.delete(url).asString();
        assertThat(response.getBody().toString(), response.getStatus(), is(HttpStatus.NO_CONTENT_204));

        HttpResponse<JsonNode> getResponse = Unirest.get(postUrl).asJson();
        assertThat(getResponse.getBody().toString(), getResponse.getStatus(), is(HttpStatus.OK_200));
        assertThat(getResponse.getBody().isArray(), is(true));
        JSONArray responseArray = getResponse.getBody().getArray();
        assertThat(responseArray.length(), is(0));
        app.stop();
    }
}
