package dev.agno.reservations;

import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.repositories.InMemoryReservationRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static dev.agno.reservations.Constants.THREE_DAYS;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

public class TestInMemoryRepository {
    @Test
    public void TestAddReservation() {
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(0);
        r.setExtent(0);
        repo.addReservation(r, r.getStartTime(), r.getExtent());
    }

    @Test
    public void TestAddDuplicateReturnsNull() {
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(0);
        r.setExtent(5);
        repo.addReservation(r, r.getStartTime(), r.getExtent());
        assertThat(repo.addReservation(r, r.getStartTime(), r.getExtent()), is(nullValue()));
    }

    @Test
    public void TestAddSequentialReservations() {
        final long earliestStartTime = 0;
        final long latestSearchTime = 10;
        final long firstIntervalExtent = 5;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent);
        String key = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(key, is(notNullValue()));
    }

    @Test
    public void TestAddBeforeOverlapReturnsNull() {
        final long earliestStartTime = 5;
        final long latestSearchTime = 10;
        final long firstIntervalExtent = 10;
        final long secondIntervalStartTime = 3;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(secondIntervalStartTime);
        assertThat(repo.addReservation(r, earliestStartTime, latestSearchTime), is(nullValue()));
    }

    @Test
    public void TestAddAfterOverlapReturnsNull() {
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(0);
        r.setExtent(5);
        repo.addReservation(r, 0, 5);
        r.setStartTime(4);
        assertThat(repo.addReservation(r, 0, 5), is(nullValue()));
    }

    @Test
    public void TestAddEnclosedOverlapReturnsNull() {
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(0);
        r.setExtent(5);
        repo.addReservation(r, 0, 5);
        r.setStartTime(2);
        r.setExtent(2);
        assertThat(repo.addReservation(r, 0, 5), is(nullValue()));
    }

    @Test
    public void TestAddEnclosingOverlapReturnsNull() {
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(2);
        r.setExtent(2);
        repo.addReservation(r, 0, 5);
        r.setStartTime(0);
        r.setExtent(5);
        assertThat(repo.addReservation(r, 0, 5), is(nullValue()));
    }

    @Test
    public void TestGetAllReservations() {
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(0);
        r.setExtent(0);
        repo.addReservation(r, 0, 0);
        var reservations = repo.getReservations(0, 10);
        assertThat(reservations, hasSize(1));
        assertThat(reservations, hasItems(r));
    }

    @Test
    public void TestGetMultipleReservations() {
        final long earliestStartTime = 0;
        final long latestSearchTime = 10;
        final long firstIntervalExtent = 5;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent);
        repo.addReservation(r, earliestStartTime, latestSearchTime);
        var reservations = repo.getReservations(earliestStartTime, latestSearchTime);
        assertThat(reservations, hasSize(2));
    }

    @Test
    public void TestDeleteNonExistentReservation() {
        final String nonExistentKey = "hello";
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        assertThat(repo.deleteReservation(nonExistentKey), is(false));
    }

    @Test
    public void TestDeleteReservation() {
        final long earliestStartTime = 0;
        final long latestSearchTime = 10;
        final long firstIntervalExtent = 5;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent);
        String secondKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(secondKey, is(notNullValue()));
        var reservations = repo.getReservations(0, 10);
        assertThat(reservations, hasSize(2));
        assertThat(repo.deleteReservation(firstKey), is(true));
        reservations = repo.getReservations(0, 10);
        assertThat(reservations, hasSize(1));
        assertThat(reservations, hasItems(r));
    }

    @Test
    public void TestUpdateReservationNonExistentKey() {
        final String nonExistentKey = "hello";
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setExtent(0);
        r.setStartTime(0);
        assertThat(repo.updateReservation(nonExistentKey, r, THREE_DAYS), is(false));
    }

    @Test
    public void TestUpdateReservationTooManyOverlaps() {
        final long earliestStartTime = 0;
        final long firstIntervalExtent = 5;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long thirdIntervalStartTime = earliestStartTime + 2 * firstIntervalExtent;
        final long updateStartTime = 3;
        final long updateExtent = 5;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation(); // 0-5
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent); // add 5-10
        String secondKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(secondKey, is(notNullValue()));
        r.setStartTime(thirdIntervalStartTime); // add 10-15
        String thirdKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(thirdKey, is(notNullValue()));
        var reservations = repo.getReservations(earliestStartTime, latestSearchTime);
        assertThat(reservations, hasSize(3));
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(thirdKey, r, THREE_DAYS), is(false));
    }

    @Test
    public void TestUpdateReservationSingleOverlapNotUpdate() {
        final long earliestStartTime = 0;
        final long firstIntervalExtent = 5;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long thirdIntervalStartTime = earliestStartTime + 2 * firstIntervalExtent;
        final long updateStartTime = 3;
        final long updateExtent = 1;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(earliestStartTime + firstIntervalExtent);
        r.setExtent(firstIntervalExtent);
        String secondKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(secondKey, is(notNullValue()));
        r.setStartTime(thirdIntervalStartTime);
        String thirdKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        assertThat(thirdKey, is(notNullValue()));
        var reservations = repo.getReservations(earliestStartTime, latestSearchTime);
        assertThat(reservations, hasSize(3));
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(thirdKey, r, THREE_DAYS), is(false));
    }

    @Test
    public void TestUpdateReservationSingleOverlapUpdate() {
        final long earliestStartTime = 0;
        final long firstIntervalExtent = 5;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long updateStartTime = 3;
        final long updateExtent = firstIntervalExtent;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(firstKey, r, THREE_DAYS), is(true));
    }

    @Test
    public void TestUpdateReservationNoOverlap() {
        final long earliestStartTime = 0;
        final long firstIntervalExtent = 5;
        final long latestSearchTime = 3 * firstIntervalExtent;
        final long updateStartTime = firstIntervalExtent + 1;
        final long updateExtent = firstIntervalExtent;
        InMemoryReservationRepository repo = new InMemoryReservationRepository();
        Reservation r = new Reservation();
        r.setStartTime(earliestStartTime);
        r.setExtent(firstIntervalExtent);
        String firstKey = repo.addReservation(r, earliestStartTime, latestSearchTime);
        r.setStartTime(updateStartTime);
        r.setExtent(updateExtent);
        assertThat(repo.updateReservation(firstKey, r, THREE_DAYS), is(true));
    }
}
