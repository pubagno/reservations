package dev.agno.reservations.domain;

import dev.agno.reservations.Constants;

import java.util.Objects;

public final class Reservation {
    private static final String FORMAT = "{Reservation: %s + %s}";
    // Start time, in seconds since epoch (Instant.getEpochSecond())
    private long startTime;

    // The number of seconds the reservation lasts
    private long extent;

    public static boolean validateReservationParameters(long startTime, long extent) {
        return startTime != Constants.BAD_STARTTIME && extent != Constants.BAD_EXTENT;
    }

    public static Reservation fromParameters(long startTime, long extent) {
        Reservation r = new Reservation();
        r.setStartTime(startTime);
        r.setExtent(extent);
        return r;
    }

    public Reservation() {}


    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getExtent() {
        return extent;
    }

    public void setExtent(long extent) {
        this.extent = extent;
    }

    public boolean overlaps(Reservation other) {
        return (this.startTime <= other.startTime && other.startTime < this.startTime + this.extent) ||
                (other.startTime <= this.startTime && this.startTime < other.startTime + other.extent);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation)obj;
        return (
                this.startTime == other.getStartTime() &&
                this.extent == other.getExtent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, extent);
    }

    @Override
    public String toString() {
        return String.format(
                FORMAT,
                startTime,
                extent);
    }
}
