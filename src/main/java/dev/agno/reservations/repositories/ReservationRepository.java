package dev.agno.reservations.repositories;

import dev.agno.reservations.domain.Reservation;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ReservationRepository {
    /**
     * Get a list of reservations that overlap with the given interval
     * @param timestamp the start of the interval, in seconds since the epoch
     * @param extent the length of the interval
     * @return
     */
    List<Reservation> getReservations(long timestamp, long extent);

    /**
     * Get the reservation with the given key
     * @param id the key for the reservation
     * @return the requested reservation, or null
     */
    Reservation getReservation(String id);

    /**
     * Add a reservation
     * @param r the reservation to add
     * @param earliestOverlap the earliest start time to search for overlaps
     * @param latestOverlap together with the earliestOverlap, defines the interval to search for conflicts
     * @return a String which is the reservation key, or null if no reservation was added
     */
    String addReservation(@NotNull Reservation r, long earliestOverlap, long latestOverlap);

    /**
     * Update the reservation for the given id to the new reservation information
     * @param id the key for the reservation
     * @param r the new reservation details
     * @param earliestOffset the amount of time, in seconds, to look for overlaps in the past
     * @return true if successful and false otherwise
     */
    boolean updateReservation(@NotNull String id, @NotNull Reservation r, long earliestOffset);

    /**
     * Delete the reservation given by the id
     * @param id the key for the reservation
     * @return true if a reservation was deleted and false otherwise
     */
    boolean deleteReservation(@NotNull String id);
}
