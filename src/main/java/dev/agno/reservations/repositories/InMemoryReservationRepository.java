package dev.agno.reservations.repositories;

import dev.agno.reservations.domain.Reservation;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public final class InMemoryReservationRepository implements ReservationRepository {
    private final TreeMap<Reservation, String> reservationsToKey;
    private final HashMap<String, Reservation> keyToReservation;

    private static class ReservationComparator implements Comparator<Reservation> {
        @Override
        public int compare(Reservation o1, Reservation o2) {
            return Long.compare(o1.getStartTime(), o2.getStartTime());
        }
    }

    public InMemoryReservationRepository() {
        this.reservationsToKey = new TreeMap<>(new ReservationComparator());
        this.keyToReservation = new HashMap<>();
    }

    @Override
    public Reservation getReservation(String id) {
        return keyToReservation.get(id);
    }

    @Override
    public List<Reservation> getReservations(long timestamp, long extent) {
        Reservation start = new Reservation();
        start.setStartTime(timestamp);
        start.setExtent(0);
        Reservation end = new Reservation();
        end.setStartTime(timestamp + extent + 1);
        end.setExtent(0);

        SortedMap<Reservation,String> reservationView = this.reservationsToKey.subMap(start, end);
        ArrayList<Reservation> result;
        if(!reservationView.isEmpty()) {
            result = new ArrayList<>(reservationView.keySet());
        }
        else {
            result = new ArrayList<>();
        }
        return result;
    }

    @Override
    public String addReservation(@NotNull Reservation r, long earliestOverlap, long latestOverlap) {
        String key = null;
        List<Reservation> overlaps = getReservations(earliestOverlap,  latestOverlap);
        boolean foundOverlap = false;
        if(!overlaps.isEmpty())
        {
            foundOverlap = overlaps.stream()
                    .anyMatch(o -> r.overlaps(o));
        }
        // else: foundOverlap is false because there were no overlaps
        if(!foundOverlap) {
            key = UUID.randomUUID().toString();
            // use a copy of the reservation object in case the caller wants to reuse it
            Reservation reservationCopy = new Reservation();
            reservationCopy.setStartTime(r.getStartTime());
            reservationCopy.setExtent(r.getExtent());
            this.reservationsToKey.put(reservationCopy, key);
            this.keyToReservation.put(key, reservationCopy);
        }

        return key;
    }

    @Override
    public boolean updateReservation(@NotNull String id, @NotNull Reservation r, long earliestOffset) {
        Reservation previous = this.keyToReservation.get(id);
        if(previous == null) {
            return false;
        }

        List<Reservation> overlaps = getReservations(
                r.getStartTime() - earliestOffset, earliestOffset + r.getExtent());
        if(overlaps.size() > 1) {
            // too many overlaps: can't possibly update
            return false;
        }
        else if((overlaps.size() == 1) && (overlaps.get(0) != previous)) {
            return false;
        }

        this.reservationsToKey.remove(previous);
        this.keyToReservation.put(id, r);
        this.reservationsToKey.put(r, id);
        return true;
    }

    @Override
    public boolean deleteReservation(@NotNull String id) {
        Reservation r = this.keyToReservation.get(id);
        if(r == null) {
            return false;
        }
        this.keyToReservation.remove(id);
        this.reservationsToKey.remove(r);
        return true;
    }
}
