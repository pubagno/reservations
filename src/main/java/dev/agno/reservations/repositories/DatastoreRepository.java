package dev.agno.reservations.repositories;

import com.google.cloud.NoCredentials;
import com.google.cloud.datastore.*;
import com.google.inject.Inject;
import dev.agno.reservations.ReservationsModule;
import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.errors.ErrorNoReservationFound;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.google.rpc.Code.ALREADY_EXISTS;
import static dev.agno.reservations.Constants.*;

public class DatastoreRepository implements ReservationRepository {
    // A reservation entity has a token, a timestamp and an extent, allowing
    // us to retrieve a reservation from a token. The key is the token.
    private static final String KIND_RESERVATION = "reservations";
    // A calendar entity has a timestamp and a token, allowing us to check for conflicts. The key is the timestamp.
    private static final String KIND_CALENDAR = "calendar";
    private static final String PROP_TIMESTAMP = "timestamp";
    private static final String PROP_EXTENT = "extent";
    private static final String PROP_TOKEN = "token";
    private Datastore datastore;

    @Inject
    public DatastoreRepository(@ReservationsModule.UseLocalDatastore boolean useLocalDatastore) {
        if (useLocalDatastore) {
            this.datastore = DatastoreOptions.newBuilder()
                    .setCredentials(NoCredentials.getInstance())
                    .setProjectId("islandreservations")
                    .setHost("http://localhost:8081")
                    .setNamespace("islandreservations")
                    .build()
                    .getService();
        }
        else {
            this.datastore = DatastoreOptions.getDefaultInstance().getService();
        }
    }

    private static Query<Entity> buildGetReservationsQuery(long timestamp, long extent) {
        return Query.newEntityQueryBuilder()
                .setKind(KIND_RESERVATION)
                .setFilter(StructuredQuery.CompositeFilter.and(
                        StructuredQuery.PropertyFilter.ge(PROP_TIMESTAMP, timestamp),
                        StructuredQuery.PropertyFilter.lt(PROP_TIMESTAMP, timestamp + extent + 1)))
                .build();
    }

    @Override
    public List<Reservation> getReservations(long timestamp, long extent) {
        Query<Entity> query = buildGetReservationsQuery(timestamp, extent);
        QueryResults<Entity> reservations = this.datastore.run(query);
        ArrayList<Reservation> result = new ArrayList<>();
        while (reservations.hasNext()) {
            Entity reservationEntity = reservations.next();
            Reservation r = new Reservation();
            r.setStartTime(reservationEntity.getLong(PROP_TIMESTAMP));
            r.setExtent(reservationEntity.getLong(PROP_EXTENT));
            result.add(r);
        }
        return result;
    }

    @Override
    public Reservation getReservation(String id) {
        Key k = datastore.newKeyFactory()
                .setKind(KIND_RESERVATION)
                .newKey(id);
        Entity e = datastore.get(k);
        Reservation result = null;
        if(e != null) {
            result = new Reservation();
            result.setStartTime(e.getLong(PROP_TIMESTAMP));
            result.setExtent(e.getLong(PROP_EXTENT));
        }
        return result;
    }

    @Override
    public String addReservation(@NotNull Reservation r, long earliestOverlap, long latestOverlap) {
        // To add a reservation, we just try to insert keys into the calendar; if it all works out, we also
        // add the reservation entry
        ArrayList<Entity> calendarEntities = new ArrayList<>();
        String token = UUID.randomUUID().toString();
        for(int offset = 0; offset < r.getExtent(); offset += ONE_DAY) {
            Key calendarKey = datastore.newKeyFactory()
                    .setKind(KIND_CALENDAR)
                    .newKey(r.getStartTime() + offset);
            calendarEntities.add(Entity.newBuilder(calendarKey)
                    .set(PROP_TOKEN, token)
                    .build());
        }
        Key reservationKey = datastore.newKeyFactory()
                .setKind(KIND_RESERVATION)
                .newKey(token);
        Entity reservationEntity = Entity.newBuilder(reservationKey)
                .set(PROP_TIMESTAMP, r.getStartTime())
                .set(PROP_EXTENT, r.getExtent())
                .build();

        Transaction txn = this.datastore.newTransaction();
        try {
            txn.add(calendarEntities.toArray(new Entity[0]));
            txn.add(reservationEntity);
            txn.commit();
        } catch(DatastoreException de) {
            if(de.getCode() == ALREADY_EXISTS.getNumber()) {
                token = null;
            }
            else {
                throw de;
            }
        } finally {
            if (txn.isActive()) {
                txn.rollback();
                token = null;
            }
        }
        return token;
    }

    @Override
    public boolean updateReservation(@NotNull String id, @NotNull Reservation r, long earliestOffset) {
        // To update a reservation, we just try to delete old keys and insert updated keys into the calendar;
        // if it all works out, we also update the reservation entry
        boolean result;
        Key reservationKey = datastore.newKeyFactory()
                .setKind(KIND_RESERVATION)
                .newKey(id);
        Entity reservationEntity = this.datastore.get(reservationKey);
        if(reservationEntity == null) {
            result = false;
        }
        else {
            long oldStartTime = reservationEntity.getLong(PROP_TIMESTAMP);
            long oldExtent = reservationEntity.getLong(PROP_EXTENT);
            ArrayList<Key> oldKeys = new ArrayList<>();
            for (int offset = 0; offset < oldExtent; offset += ONE_DAY) {
                oldKeys.add(datastore.newKeyFactory()
                        .setKind(KIND_CALENDAR)
                        .newKey(oldStartTime + offset));
            }

            ArrayList<Entity> calendarEntities = new ArrayList<>();
            for (int offset = 0; offset < r.getExtent(); offset += ONE_DAY) {
                Key calendarKey = datastore.newKeyFactory()
                        .setKind(KIND_CALENDAR)
                        .newKey(r.getStartTime() + offset);
                calendarEntities.add(Entity.newBuilder(calendarKey)
                        .set(PROP_TOKEN, id)
                        .build());
            }

            reservationEntity = Entity.newBuilder(reservationKey)
                    .set(PROP_TIMESTAMP, r.getStartTime())
                    .set(PROP_EXTENT, r.getExtent())
                    .build();

            Transaction txn = this.datastore.newTransaction();
            try {
                txn.delete(oldKeys.toArray(new Key[0]));
                txn.add(calendarEntities.toArray(new Entity[0]));
                txn.update(reservationEntity);
                txn.commit();
                result = true;
            } catch (DatastoreException de) {
                if (de.getCode() == ALREADY_EXISTS.getNumber()) {
                    result = false;
                } else {
                    throw de;
                }
            } finally {
                if (txn.isActive()) {
                    txn.rollback();
                    result = false;
                }
            }
        }
        return result;
    }

    @Override
    public boolean deleteReservation(@NotNull String id) {
        boolean result;
        ArrayList<Key> toDelete = new ArrayList<>();
        Key reservationKey = datastore.newKeyFactory()
                .setKind(KIND_RESERVATION)
                .newKey(id);
        Entity reservationEntity = this.datastore.get(reservationKey);
        if(reservationEntity == null) {
            result = false;
        }
        else {
            long startTime = reservationEntity.getLong(PROP_TIMESTAMP);
            long extent = reservationEntity.getLong(PROP_EXTENT);

            for (int offset = 0; offset < extent; offset += ONE_DAY) {
                Key calendarKey = datastore.newKeyFactory()
                        .setKind(KIND_CALENDAR)
                        .newKey(startTime + offset);
                toDelete.add(calendarKey);
            }
            toDelete.add(reservationKey);

            Transaction txn = this.datastore.newTransaction();
            try {
                txn.delete(toDelete.toArray(new Key[0]));
                txn.commit();
                result = true;
            } finally {
                if (txn.isActive()) {
                    txn.rollback();
                    result = false;
                }
            }
        }
        return result;
    }
}
