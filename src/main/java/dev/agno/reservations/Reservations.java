package dev.agno.reservations;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

public class Reservations {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(ReservationsModule.create());
        /*
        Javalin app = Javalin.create(config -> {
            config.registerPlugin(new OpenApiPlugin(getOpenApiOptions()));
        });
        ReservationRepository repository = new InMemoryReservationRepository();
        ReservationService service = new DefaultReservationService(repository);
        ReservationRouter router = new ReservationRouter(app, service);
        router.bindRoutes();

         */
        Javalin app = injector.getInstance(Javalin.class);
        ReservationRouter router = injector.getInstance(ReservationRouter.class);
        router.bindRoutes();
        ReservationsModule.ListenPortProvider portProvider = injector.getInstance(ReservationsModule.ListenPortProvider.class);
        app.start(portProvider.getPort());
    }

    private static OpenApiOptions getOpenApiOptions() {
        Info applicationInfo = new Info()
                .version("1.0")
                .description("Reservations");
        return new OpenApiOptions(applicationInfo)
                .path("/swagger-docs")
                .swagger(new SwaggerOptions("/swagger").title("Reservations swagger docs"))
        ;
    }
}
