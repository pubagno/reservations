package dev.agno.reservations;

import com.google.inject.Inject;
import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.errors.*;
import dev.agno.reservations.services.ReservationService;
import io.javalin.Javalin;
import io.javalin.http.*;
import io.javalin.plugin.openapi.annotations.*;
import org.eclipse.jetty.http.HttpStatus;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static dev.agno.reservations.Constants.*;
import static dev.agno.reservations.domain.Reservation.validateReservationParameters;
import static io.javalin.apibuilder.ApiBuilder.*;

public class ReservationRouter {
    private final ReservationService service;
    private final Javalin javalin;

    @Inject
    public ReservationRouter(@NotNull Javalin javalin, @NotNull ReservationService service) {
        this.javalin = javalin;
        this.service = service;
    }

    public void bindRoutes() {
        javalin
            .routes(() -> {
            path(RESERVATIONS_PREFIX, () -> {
                get(this::getReservations);
                post(this::postReservation);
                path(RESERVATIONS_KEY_PATH, () -> {
                   patch(this::patchReservation);
                   get(this::getReservation);
                   delete(this::deleteReservation);
                });
            });
        })
            .exception(ErrorReservationOneDayAhead.class, this::translateToBadRequest)
            .exception(ErrorReservationPastOneMonth.class, this::translateToBadRequest)
            .exception(ErrorStartTimeNotMidnight.class, this::translateToBadRequest)
            .exception(ErrorEndTimeNotDayMultiple.class, this::translateToBadRequest)
            .exception(ErrorReservationTooLong.class, this::translateToBadRequest)
            .exception(ErrorUpdateConflict.class, this::translateToConflict)
            .exception(ErrorNoReservationFound.class, this::translateToNotFound)
        ;
    }

    private void translateToBadRequest(Exception e, Context ctx) {
        ctx.status(HttpStatus.BAD_REQUEST_400);
        ctx.json(e.getMessage());
    }

    private void translateToConflict(Exception e, Context ctx) {
        ctx.status(HttpStatus.CONFLICT_409);
        ctx.json(e.getMessage());
    }

    private void translateToNotFound(Exception e, Context ctx) {
        ctx.status(HttpStatus.NOT_FOUND_404);
        ctx.json(e.getMessage());
    }

    private static long extractMidnightStartTime(@NotNull Context ctx) {
        long initialStartTime = extractStartTime(ctx);
        long midnight = Instant.ofEpochSecond(initialStartTime).truncatedTo(ChronoUnit.DAYS).getEpochSecond();
        if(midnight != initialStartTime) {
            throw new ErrorStartTimeNotMidnight(String.format("Start time must be midnight UTC (try %d)\n", midnight));
        }
        return initialStartTime;
    }

    private static long extractStartTime(@NotNull Context ctx) {
        return ctx.queryParam(PARAM_STARTTIME, Long.class, BAD_STARTTIME_STRING).get();
    }

    private static long extractDayLengthExtent(@NotNull Context ctx) {
        long initialExtent = extractExtent(ctx);
        if(initialExtent % ONE_DAY != 0) {
            long suggestedExtent = ((initialExtent + ONE_DAY) / ONE_DAY) * ONE_DAY;
            throw new ErrorEndTimeNotDayMultiple(
                    String.format(
                            "Reservation length must be a multiple of a day, in seconds (try %d)\n", suggestedExtent));
        }
        return initialExtent;
    }

    private static long extractExtent(@NotNull Context ctx) {
        return ctx.queryParam(PARAM_EXTENT, Long.class, BAD_EXTENT_STRING).get();
    }

    private static Reservation createReservationOrThrow(@NotNull Context ctx) {
        long startTime = extractMidnightStartTime(ctx);
        long extent = extractDayLengthExtent(ctx);
        if(!validateReservationParameters(startTime, extent)) {
            throw new BadRequestResponse("Invalid reservation parameters\n");
        }
        return Reservation.fromParameters(startTime, extent);
    }

    @OpenApi(
            path=RESERVATIONS_PREFIX,
            method = HttpMethod.GET,
            description="Get the reservations, by default from now to a month from now",
            queryParams = {
                    @OpenApiParam(
                            name="startTime",
                            type=Long.class,
                            description = "The start time, in seconds since the epoch, of the interval in which to search for reservations"
                    ),
                    @OpenApiParam(
                            name ="extent",
                            type=Long.class,
                            description = "The extent, in seconds, of the interval in which to search for reservations"
                    )
            },
            responses = {
                    @OpenApiResponse(
                            status="200",
                            content=@OpenApiContent(from = Reservation.class, isArray = true, type = MIME_TYPE_JSON)
                    )
            }
    )
    private void getReservations(Context ctx) {
        long startTime = extractStartTime(ctx);
        long extent = extractExtent(ctx);
        if(startTime == BAD_STARTTIME) {
            startTime = Instant.now().getEpochSecond();
        }
        if(extent == BAD_EXTENT) {
            extent = DEFAULT_EXTENT;
        }
        List<Reservation> reservations = this.service.getReservations(startTime, extent);
        ctx.json(reservations);
    }

    @OpenApi(
            path = RESERVATIONS_PREFIX,
            method = HttpMethod.POST,
            description = "Create a reservation",
            queryParams = {
                    @OpenApiParam(
                            name = "startTime",
                            type = Long.class,
                            description = "The start time, in seconds since epoch, of the reservation. Must be a midnight start time",
                            required = true
                    ),
                    @OpenApiParam(
                            name = "extent",
                            type = Long.class,
                            description = "The amount of time, in seconds, for the reservation. Must be a multiple of 86400 (a day)",
                            required = true
                    )
            },
            responses = {
                    @OpenApiResponse(
                            status = "201",
                            description = "Return 201 Created on successful creation; response body contains the key for the reservation",
                            content = @OpenApiContent(from = String.class, type=MIME_TYPE_JSON)
                    ),
                    @OpenApiResponse(
                            status = "409",
                            description = "Return 409 Conflict if the reservation conflicts with existing reservations"
                    ),
                    @OpenApiResponse(
                            status = "400",
                            description = "Bad request on incorrect parameters, startTime not 1 day in advance, or farther than 1 month out, or extent greater than 3 days",
                            content = @OpenApiContent(from = String.class, type=MIME_TYPE_JSON)
                    )
            }
    )
    private void postReservation(Context ctx) {
        Reservation toAdd = createReservationOrThrow(ctx);
        String key = this.service.makeReservation(toAdd);
        if(key == null) {
            throw new ConflictResponse("Unable to add reservation\n");
        }
        ctx.status(HttpStatus.CREATED_201);
        ctx.json(key);
    }

    @OpenApi(
            path = RESERVATIONS_PREFIX + "/" + RESERVATIONS_KEY_PATH,
            method = HttpMethod.PATCH,
            description="Update an existing reservation",
            pathParams = {
                    @OpenApiParam(
                            name = PARAM_KEY,
                            description = "The key for the reservation, as returned via a POST",
                            required = true
                    )
            },
            queryParams = {
                    @OpenApiParam(
                            name = "startTime",
                            type = Long.class,
                            description = "The start time, in seconds since epoch, of the updated reservation. Must be a midnight start time",
                            required = true
                    ),
                    @OpenApiParam(
                            name = "extent",
                            type = Long.class,
                            description = "The amount of time, in seconds, for the updated reservation. Must be a multiple of 86400 (a day)",
                            required = true
                    )
            },
            responses = {
                    @OpenApiResponse(
                            status="204",
                            description="Return 204 No Content on a successful update"
                    ),
                    @OpenApiResponse(
                            status="409",
                            description="Return 409 Conflict if the updated reservation would conflict with an existing one"
                    ),
                    @OpenApiResponse(
                            status="400",
                            description="Returns 400 Bad Request on errors, with same errors as during a POST",
                            content = @OpenApiContent(from = String.class, type=MIME_TYPE_JSON)
                    )
            }
    )
    private void patchReservation(Context ctx) {
        String key = ctx.pathParam(PARAM_KEY);
        Reservation updatedReservation = createReservationOrThrow(ctx);
        boolean wasUpdated = this.service.updateReservation(key, updatedReservation);
        if (wasUpdated) {
            ctx.status(HttpStatus.NO_CONTENT_204);
        }
        else {
            throw new BadRequestResponse("Unable to update reservation\n");
        }
    }

    @OpenApi(
            path = RESERVATIONS_PREFIX + "/" + RESERVATIONS_KEY_PATH,
            method = HttpMethod.DELETE,
            description="Delete an existing reservation",
            pathParams = {
                    @OpenApiParam(
                            name = PARAM_KEY,
                            description = "The key for the reservation, as returned via a POST",
                            required = true
                    )
            },
            responses = {
                    @OpenApiResponse(
                            status="204",
                            description="Return 204 No Content on a successful update"
                    ),
                    @OpenApiResponse(
                            status="404",
                            description="Return 404 Not Found if the key does not match an existing reservation"
                    ),
                    @OpenApiResponse(
                            status="400",
                            description="Returns 400 Bad Request on errors",
                            content = @OpenApiContent(from = String.class, type=MIME_TYPE_JSON)
                    )
            }
    )
    private void deleteReservation(Context ctx) {
        String key = ctx.pathParam(PARAM_KEY);
        boolean wasDeleted = this.service.cancelReservation(key);
        if (wasDeleted) {
            ctx.status(HttpStatus.NO_CONTENT_204);
        }
        else {
            throw new NotFoundResponse("Unable to delete reservation\n");
        }
    }

    @OpenApi(
            path = RESERVATIONS_PREFIX + "/" + RESERVATIONS_KEY_PATH,
            method = HttpMethod.GET,
            description="Get details of an existing reservation",
            pathParams = {
                    @OpenApiParam(
                            name = PARAM_KEY,
                            description = "The key for the reservation, as returned via a POST",
                            required = true
                    )
            },
            responses = {
                    @OpenApiResponse(
                            status="200",
                            description="Return 200 with reservation details",
                            content = @OpenApiContent(from = Reservation.class, type=MIME_TYPE_JSON)
                    ),
                    @OpenApiResponse(
                            status="404",
                            description="Return 404 Not Found if the key does not match an existing reservation"
                    ),
                    @OpenApiResponse(
                            status="400",
                            description="Returns 400 Bad Request on errors",
                            content = @OpenApiContent(from = String.class, type=MIME_TYPE_JSON)
                    )
            }
    )
    private void getReservation(Context ctx) {
        String key = ctx.pathParam(PARAM_KEY);
        Reservation r = this.service.getReservation(key);
        if(r == null) {
            ctx.status(HttpStatus.NOT_FOUND_404);
        }
        else {
            ctx.json(r);
        }
    }
}
