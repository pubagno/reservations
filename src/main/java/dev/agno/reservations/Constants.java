package dev.agno.reservations;

public class Constants {
    // 30 day default extent
    public final static String DEFAULT_EXTENT_STRING = "2592000";
    public final static String DEFAULT_STARTTIME_STRING = "-1";
    public final static String BAD_STARTTIME_STRING = DEFAULT_STARTTIME_STRING;
    public final static String BAD_EXTENT_STRING = "0";
    public final static String PARAM_STARTTIME = "startTime";
    public final static String PARAM_EXTENT = "extent";
    public final static String PARAM_KEY = "key";
    public final static long BAD_STARTTIME = -1;
    public final static long BAD_EXTENT = 0;
    public final static long DEFAULT_EXTENT = 2592000;
    public final static long MAX_SEARCH_HISTORY = DEFAULT_EXTENT;
    public final static long ONE_DAY = 24 * 3600;
    public final static long THREE_DAYS = 3 * ONE_DAY;

    public final static String RESERVATIONS_PREFIX = "v1/reservations";
    public final static String RESERVATIONS_KEY_PATH = ":key";

    public final static String MIME_TYPE_JSON = "application/json";
    public final static String PROP_REPOSITORY_CLASS = "repository";
    public final static String PROP_REDIS_HOSTS = "redis_hosts";

    public final static String ENV_PORT = "ISLANDRESERVATIONS_PORT";
    public final static String ENV_REPOSITORY_CLASS = "ISLANDRESERVATIONS_REPO";
    public final static String ENV_USE_LOCAL_DATASTORE = "ISLANDRESERVATIONS_USELOCALDATASTORE";
    public final static int DEFAULT_PORT = 8002;
}
