package dev.agno.reservations.errors;

public class ErrorReservationTooLong extends RuntimeException {
    public ErrorReservationTooLong(String msg) {
        super(msg);
    }
}
