package dev.agno.reservations.errors;

public class ErrorStartTimeNotMidnight extends RuntimeException {
    public ErrorStartTimeNotMidnight(String msg) {
        super(msg);
    }
}
