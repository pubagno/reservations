package dev.agno.reservations.errors;

public class ErrorReservationOneDayAhead extends RuntimeException {
    public ErrorReservationOneDayAhead(String msg) {
        super(msg);
    }
}
