package dev.agno.reservations.errors;

public class ErrorUpdateConflict extends RuntimeException {
    public ErrorUpdateConflict(String msg) {
        super(msg);
    }
}
