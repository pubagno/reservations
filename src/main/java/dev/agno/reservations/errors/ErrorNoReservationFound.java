package dev.agno.reservations.errors;

public class ErrorNoReservationFound extends RuntimeException {
    public ErrorNoReservationFound(String msg) {
        super(msg);
    }
}
