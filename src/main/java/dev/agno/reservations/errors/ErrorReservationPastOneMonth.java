package dev.agno.reservations.errors;

public class ErrorReservationPastOneMonth extends RuntimeException {
    public ErrorReservationPastOneMonth(String msg) {
        super(msg);
    }
}
