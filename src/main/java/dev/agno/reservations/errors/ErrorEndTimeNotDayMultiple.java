package dev.agno.reservations.errors;

public class ErrorEndTimeNotDayMultiple extends RuntimeException {
    public ErrorEndTimeNotDayMultiple(String msg) {
        super(msg);
    }
}
