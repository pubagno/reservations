package dev.agno.reservations.services;

import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.errors.ErrorReservationOneDayAhead;
import dev.agno.reservations.errors.ErrorReservationPastOneMonth;

import java.util.List;

public interface ReservationService {
    /**
     * Get a list of reservations that overlap the given interval
     * @param startTime: the start of the interval, in seconds since the epoch
     * @param extent: the length of the interval, in seconds
     * @return a List of Reservation objects that overlap
     */
    List<Reservation> getReservations(long startTime, long extent);

    /**
     * Get the reservation for the given key
     * @param key the key for the reservation
     * @return The requested Reservation, or null if no such reservation exists
     */
    Reservation getReservation(String key);

    /**
     * Make a new reservation
     * @param r the reservation to make
     * @return a String key if the reservation was made, or null
     */
    String makeReservation(Reservation r) throws ErrorReservationOneDayAhead, ErrorReservationPastOneMonth;

    /**
     * Update an existing reservation
     * @param key: the reservation key to update
     * @param r: the updated reservation details
     * @return true if the update was successful and false otherwise
     */
    boolean updateReservation(String key, Reservation r);

    /**
     * Cancel an existing reservation
     * @param key: the reservation key to cancel
     * @return true if the cancellation was successful and false otherwise
     */
    boolean cancelReservation(String key);
}
