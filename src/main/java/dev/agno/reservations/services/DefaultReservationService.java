package dev.agno.reservations.services;

import com.google.inject.Inject;
import dev.agno.reservations.domain.Reservation;
import dev.agno.reservations.errors.*;
import dev.agno.reservations.repositories.ReservationRepository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static dev.agno.reservations.Constants.*;

public final class DefaultReservationService implements ReservationService {
    private final ReservationRepository repository;

    @Inject
    public DefaultReservationService(ReservationRepository repo) {
        this.repository = repo;
    }

    @Override
    public List<Reservation> getReservations(long startTime, long extent) {
        return this.repository.getReservations(startTime, extent);
    }

    @Override
    public Reservation getReservation(String key) {
        return this.repository.getReservation(key);
    }

    @Override
    public String makeReservation(Reservation r) throws ErrorReservationOneDayAhead, ErrorReservationPastOneMonth {
        long now = Instant.now().getEpochSecond();
        // Reservations need to be made at least 1 day in advance
        if(r.getStartTime() - now < ONE_DAY) {
            throw new ErrorReservationOneDayAhead("Reservation must be made at least one day in advance\n");
        }
        LocalDateTime localNow = LocalDateTime.now();
        long monthFromNow = localNow.plusMonths(1).toEpochSecond(ZoneOffset.UTC);
        if(monthFromNow < r.getStartTime()) {
            throw new ErrorReservationPastOneMonth("Reservations can only be made one month in advance\n");
        }
        if(r.getExtent() > THREE_DAYS) {
            throw new ErrorReservationTooLong("Reservations can only be up to 3 days\n");
        }
        return this.repository.addReservation(
                r, r.getStartTime() - THREE_DAYS, r.getExtent() + THREE_DAYS);
    }

    @Override
    public boolean updateReservation(String key, Reservation r) {
        if(this.repository.getReservation(key) == null) {
            throw new ErrorNoReservationFound("No such reservation exists\n");
        }
        if(!this.repository.updateReservation(key, r, THREE_DAYS))
            throw new ErrorUpdateConflict("Updated reservation would conflict with existing one\n");
        return true;
    }

    @Override
    public boolean cancelReservation(String key) {
        return this.repository.deleteReservation(key);
    }
}
