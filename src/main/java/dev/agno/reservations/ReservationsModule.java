package dev.agno.reservations;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import dev.agno.reservations.repositories.InMemoryReservationRepository;
import dev.agno.reservations.repositories.ReservationRepository;
import dev.agno.reservations.services.DefaultReservationService;
import dev.agno.reservations.services.ReservationService;
import io.javalin.Javalin;
import org.jetbrains.annotations.NotNull;

import javax.inject.Qualifier;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Properties;

import static dev.agno.reservations.Constants.*;

public class ReservationsModule extends AbstractModule {
    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @interface ListenPort {}

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    public @interface UseLocalDatastore {}

    private Javalin app;

    private ReservationsModule(Javalin app) {
        this.app = app;
    }

    @NotNull
    public static ReservationsModule create() {
        return new ReservationsModule(Javalin.create());
    }

    private Properties getConfig() {
        Properties prop = new Properties();
        String fileName = "config.properties";
        prop.setProperty(PROP_REPOSITORY_CLASS, InMemoryReservationRepository.class.getName());
        try (InputStream input = new FileInputStream(fileName)) {
            prop.load(input);
        }
        catch(IOException ex) {
            System.err.println("Unable to load config.properties file");
        }
        return prop;
    }

    static String getRepositoryName(Properties props) {
        String result = props.getProperty(PROP_REPOSITORY_CLASS);
        String envString = System.getenv(ENV_REPOSITORY_CLASS);
        if(envString != null) {
            result = envString;
        }
        return result;
    }

    @Provides
    @ListenPort
    static int provideListenPort() {
        String portString = System.getenv(ENV_PORT);
        int port = DEFAULT_PORT;
        if(portString != null) {
            port = Integer.parseInt(portString);
        }
        return port;
    }

    @Provides
    @UseLocalDatastore
    static boolean provideUseLocalDatastore() {
        String useLocalString = System.getenv(ENV_USE_LOCAL_DATASTORE);
        boolean result = false;
        if(useLocalString != null) {
            result = Boolean.parseBoolean(useLocalString);
        }
        return result;
    }

    static class ListenPortProvider {
        private int port;

        @Inject
        ListenPortProvider(@ListenPort int port) {
            this.port = port;
        }

        int getPort() {
            return this.port;
        }
    }

    @Override
    protected void configure() {
        bind(Javalin.class).toInstance(app);
        Properties prop = getConfig();
        try {
            Class repoClass = Class.forName(getRepositoryName(prop));
            bind(ReservationRepository.class).to(repoClass);
        }
        catch(ClassNotFoundException e) {
            throw new RuntimeException("Unable to load repository class");
        }
        bind(ReservationService.class).to(DefaultReservationService.class);
        bind(ReservationRouter.class);
        bind(ListenPortProvider.class);
    }
}
