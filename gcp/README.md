To enable workload identity:

Follow instructions for cluster setup here: https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
kubectl create serviceaccount --namespace default irksa
gcloud iam service-accounts create islandreservationsgsa
kubectl annotate serviceaccount --namespace default irksa iam.gke.io/gcp-service-account=islandreservationsgsa@islandreservations.iam.gserviceaccount.com
gcloud iam service-accounts add-iam-policy-binding --role roles/iam.workloadIdentityUser --member "serviceAccount:islandreservations.svc.id.goog[default/irksa]" islandreservationsgsa@islandreservations.iam.gserviceaccount.com

docker build --no-cache -t gcr.io/islandreservations/islandreservations:0.0.3 -f container/Dockerfile
docker push gcr.io/islandreservations/islandreservations:0.0.3

kubectl create deployment irdeployment --image=gcr.io/islandreservations/islandreservations:0.0.3

Add `serviceAccountName: irksa` to deployment spec
